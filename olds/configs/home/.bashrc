#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

# beeo off
xset b off


# alias

alias ls='ls --color=auto'
# alias sbcl='ros use sbcl; ros run'
alias lem-rxvt='urxvt -ls -hold -e "lem"'
alias next-self='exec "~/.next/next"'
alias la="ls -la"
alias shutdown-now="sudo shutdown now -h"


# style
export LSCOLORS=gxfxcxdxbxegedabagaca

# paths
export PATH=~/.roswell/bin:$PATH
export PATH=~/.config/scripts:$PATH
export PATH=~/.local/bin:$PATH
export PATH=~/.gem/ruby/2.6.0/bin:$PATH

# games
export GEARHEAD_LANG=ja_JP.eucJP