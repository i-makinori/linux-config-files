

(in-package :lem-user)

(ql:quickload 'inferior-shell)


;; global

(setf *scroll-recenter-p* nil)
(setf (variable-value 'truncate-lines :global) t)


;; lem-shell


;; lem-mcclim


;; runcher

(define-command urxvt () ()
  (uiop:run-program (list "urxvt-and")))

(define-command next-browser () ()
  (uiop:run-program (list "next-and" )))

(define-command lem-new-buffer () ()
  (uiop:run-program (list "lemrxvt-and" )))

;; clipboard

(defun xclip-read-clipboard-string ()
  (inferior-shell:run/ss '(xclip -o -selection clipboard)))

(defun xclip-set-clipboard-string (string)
  (inferior-shell:run/ss
   `(inferior-shell:pipe (echo (,string)) (xclip -in -selection clipboard))))


(define-key *global-keymap* "M-w" 'copy-to-xclipboard)
(define-command copy-to-xclipboard (start end) ("r")
  (unless (continue-flag :kill)
    (kill-ring-new))
  (let ((marked-string (points-to-string start end)))
    (xclip-set-clipboard-string marked-string)
    (kill-push marked-string)
    (message (format nil "copy from x-clipboard and killpush"))
    )
  (buffer-mark-cancel (current-buffer)))

 
(define-key *global-keymap* "M-y" 'yank-from-xclipboard)
(define-command yank-from-xclipboard (&optional arg) ("P")
  (declare (ignore arg))
  (message (format nil "yank from x-clipboard"))
  (setf (buffer-value (current-buffer) 'yank-start) (copy-point (current-point) :temporary))
  (let ((string (xclip-read-clipboard-string)))
    (insert-string (current-point) string))
  (setf (buffer-value (current-buffer) 'yank-end) (copy-point (current-point) :temporary))
  (continue-flag :yank)
  t)
















































