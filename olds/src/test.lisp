;; #!/bin/sh
#|-*- mode:lisp -*-|#
#|
exec ros -Q -- $0 "$@"
|#

;;; package

(progn
  (ros:ensure-asdf)
  ;;#+quicklisp(ql:quickload '() :silent t)
  (ql:quickload :cl-fad))
  

(defpackage :ros.script.util.linux-config-files.util
  (:use :cl :cl-fad))
(in-package :ros.script.util.linux-config-files.util)

(defun main (&rest argv)
  (declare (ignorable argv))
  (progn 
    (format t "~%--- update config files linked from symlinks/ to configs/~%")
    (let* ((state-string-list
             (cp-configs-of-symlinked-source-from-symlinks-link-source)))
      (mapcar #'(lambda (state-str)
                  (format t "~A~%" state-str))
              state-string-list))
    (format t "~%--- quit ---~%")
    nil)
  nil)


;;; configs

(defparameter *current-directory*
  *default-pathname-defaults*)
(defparameter *symlinks-directory*
  (merge-pathnames-as-directory *current-directory* "symlinks/"))
(defparameter *configs-directory*
  (merge-pathnames-as-directory *current-directory* "configs/"))


;; links

#|
(defmacro dire-link (target &key (name nil) links)
  "link of directory"
  `(let ((link-dires-name ,(if name name target)))
     (list ,target link-dires-name
           ,(mapcar #'(lambda (path)
                       (funcall (car path) (cdr path)))
                   links)))
  )

(defmacro file-link (target &key (name nil))
  "link of file"
  `(let ((link-files-name ,(if name name target)))
     (list ,target link-files-name)
  ))

(defun link-tree (link-tree)
  link-tree
  )


(defparameter *relation-list*
  (link-tree
   (dire-link
    "$HOME" :name *configs-directory*
    ((file-link ".Xmodmap")
     (file-link ".bash_profile")
     (file-link ".bashrc")
     (file-link ".xinitrc")
     (file-link ".xprofile")
     )
    (file-link "aaa")
    ))
   
   
  )
|#


#|
(defun link-tree (link-tree)
  (cond
    ((null link-tree) '())
    ((eq (car link-tree) :dire)
     )
    ((eq (car link-tree) :file) :file)
    
  ))


(defparameter *relation-list*
  (link-tree
   '(:dire
     "$HOME" :name *configs-directory*
     ((:file ".Xmodmap")
      (:file ".bash_profile")
      (:file ".bashrc")
      (:file ".xinitrc")
      (:file ".xprofile")
      (:dire
       ".config" :name "dot_config"
       ((:dire "rofi"
         ((:file "config")
          (:dire "themes"
           ((:file "glue_pro_blue.rasi")))))
        (:dire "scripts"
         ((:file "urxvt-and"))
         ((:file "lemrxvt-and")))))
     ))))
   
|#


  
  


;; rewrite

(defun pathname-symlinks-to-configs (symlinks-pathname)
  (merge-pathnames (enough-namestring symlinks-pathname *symlinks-directory*) *configs-directory*))

(defun cp-symlinks-links-file-to-strucuted-config-dir (symlinks-pathname)
  (let ((pathname-under-config-dir (pathname-symlinks-to-configs symlinks-pathname)))
    (if (directory-pathname-p symlinks-pathname)
        (cond ((symlink-path// symlinks-pathname)
               nil
               (format nil "ignore symlink_dir : ~A" symlinks-pathname))
              ((probe-file pathname-under-config-dir)
               nil
               (format nil "aleady exist : ~A" symlinks-pathname))
              (t (ensure-directories-exist pathname-under-config-dir)
                 (format nil "new directory : ~A" symlinks-pathname)))
        (cond ((probe-file pathname-under-config-dir)
               (copy-file (symlink-path// symlinks-pathname) pathname-under-config-dir :overwrite t)
               (format nil "rewrite-file : ~A" symlinks-pathname))
            (t
             (copy-file (symlink-path// symlinks-pathname) pathname-under-config-dir :overwrite nil)
             (format nil "new copy file : ~A" symlinks-pathname))))))

(defun cp-configs-of-symlinked-source-from-symlinks-link-source ()
  (let* ((list-leaf-of-symlinks-directory
           (filter-directory-recursiv-list// *symlinks-directory* #'(lambda (x) x t)))
         (state-string-list
           (mapcar
            #'(lambda (gen-path)
                (cp-symlinks-links-file-to-strucuted-config-dir gen-path))
            list-leaf-of-symlinks-directory)))
    state-string-list))



;;; cl-fat

(defun symlink-path// (path)
  (if (not (pathname-equal path (probe-file path)))
      (probe-file path)))

(defun filter-directory-recursiv-list// (dir predicate)
  ;; idea by walk-directory
  ;; without follow symlinks
  (labels
      ((walk (stack ls-log-list)
         (cond ((null stack) ls-log-list)
               (t (let* ((ls-current-directory
                           (list-directory (car stack) :follow-symlinks nil))
                         (stack-push
                           (remove-if
                            #'(lambda (path)
                                (or (symlink-path// path) ;; apply symlink parameter
                                    (not (directory-pathname-p path))
                                     (not (funcall predicate path))))
                            ls-current-directory)))
                    (walk
                     (append stack-push (cdr stack))
                     (append ls-current-directory ls-log-list)))))))
    (reverse (walk (list dir) '()))))

