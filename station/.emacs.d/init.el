
;;;; init

(setq ring-bell-function 'ignore) ;;bell
(setq make-backup-files nil) ;;file
(setq auto-save-default nil)
(set-language-environment 'Japanese) ;; encode
(prefer-coding-system 'utf-8)
(setq-default indent-tabs-mode nil) ;; indent

(setq inhibit-startup-screen t) ;; ignore startup
(show-paren-mode t) ;; paren
(menu-bar-mode 0) ;; window-bars
(tool-bar-mode 0)

(global-set-key (kbd "C-h") 'delete-backward-char) ;; keys
(global-set-key (kbd "M-p") 'scroll-down)
(global-set-key (kbd "M-n") 'scroll-up)
(global-set-key (kbd "C-\\") 'undo)
(global-set-key (kbd "C-2") 'set-mark-command)


;;;; package
(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
        ("melpa" . "http://melpa.org/packages/")
        ("melpa-stable" . "https://stable.melpa.org/packages/")
        ("org" . "http://orgmode.org/elpa/")))
(package-initialize)

;;;; scroll

(setq scroll-margin 1)
(setq scroll-conservatively 1)
(setq next-screen-context-lines 10)

;;;; theme


(set-frame-font "ricty-13")

(setq-default show-trailing-whitespace t) ;; highlight
(add-hook 'font-lock-mode-hook            ;; highlight
          (lambda ()
            (font-lock-add-keywords
             nil
             '(("\t" 0 'trailing-whitespace prepend)))))

;;;; popwin

(require 'popwin)
(setq display-buffer-function 'popwin:display-buffer)
(setq popwin:popup-window-position 'bottom)

;; buffer


;;;; helm
(require 'helm)
(helm-mode t)
(global-set-key (kbd "M-x") 'helm-M-x) ;; key bindings
(define-key global-map (kbd "C-x C-f") 'helm-find-files)
(define-key helm-map (kbd "C-h") 'delete-backward-char)
(define-key helm-find-files-map (kbd "C-h") 'delete-backward-char)
(define-key helm-read-file-map (kbd "TAB") 'helm-execute-persistent-action)
(define-key helm-find-files-map (kbd "TAB") 'helm-execute-persistent-action)


;;;; sudo open file
(defun sudo-find-file (file-name)
  "Like find file, but opens the file as root."
  (interactive "FSudo Find File: ")
  (let ((tramp-file-name (concat "/sudo::" (expand-file-name file-name))))
    (find-file tramp-file-name)))

;; shell
(setq shell-command-switch "-ic")

;; (load-file (expand-file-name "~/.emacs.d/shellenv.el"))
(dolist (path (reverse (split-string (getenv "PATH") ":")))
  (add-to-list 'exec-path path))


;;;; modeline
(require 'smart-mode-line)

(setq sml/active-background-color "gray60")

(column-number-mode t)
(line-number-mode t)

(setq sml/read-only-char "%%")
(setq sml/modified-char "*")

(setq sml/hidden-modes '(" Helm" " AC"))
(setq sml/extra-filler -10)

(add-to-list 'sml/replacer-regexp-list '("^.+/junk/[0-9]+/" ":J:") t)

(setq sml/no-confirm-load-theme t)
(sml/setup)
(sml/apply-theme 'light)


;; company
;; (autoload 'company)
;; (autoload 'company-quickhelp)
(with-eval-after-load 'company
  (company-quickhelp-mode +1)
  (setq company-quickhelp-delay 0.28)
  (setq company-auto-expand t)
  ;; (setq company-idle-delay 0)
  
  (setq company-selection-wrap-around t)
  (setq completion-ignore-case t)
  (setq company-dabbrev-downcase nil)
  (setq company-minimum-prefix-length 1)
  (global-set-key (kbd "C-M-i") 'company-complete)
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous)
  (define-key company-active-map [tab] 'company-complete-selection)
  (define-key company-active-map (kbd "C-h") nil)
  (define-key company-active-map (kbd "C-S-h") 'company-show-doc-buffer)

  (set-face-attribute 'company-tooltip nil
                      :foreground "black" :background "lightgrey")
  (set-face-attribute 'company-tooltip-common nil
                      :foreground "black" :background "lightgrey")
  (set-face-attribute 'company-tooltip-common-selection nil
                      :foreground "white" :background "steelblue")
  (set-face-attribute 'company-tooltip-selection nil
                      :foreground "black" :background "steelblue")
  (set-face-attribute 'company-preview-common nil
                      :background nil :foreground "lightgrey" :underline t)
  (set-face-attribute 'company-scrollbar-fg nil
                      :background "orange")
  (set-face-attribute 'company-scrollbar-bg nil
                      :background "gray40")
  )

(global-company-mode 1)
(setq company-global-modes '(not org-mode))
(setq company-global-modes '(not eshell-mode))


;;; skk
(require 'skk)
(setq default-input-method "japanese-skk")
(global-set-key (kbd "C-SPC") 'skk-mode)
(global-set-key "\C-x\C-j" 'skk-mode)
(global-set-key "\C-xj" 'skk-auto-fill-mode)
(global-set-key "\C-xt" 'skk-tutorial)
(setq skk-large-jisyo "/usr/share/skk/SKK-JISYO.L")


;;;; w3m

;;;; orgmode



;;;; haskell

;(package-install 'intero)
;(add-hook 'haskell-mode-hook 'intero-mode)


;;;; scheme


;;(use-package geiser
;;             :config
;;             (setq geiser-active-implementations '(racket)))

(setq geiser-racket-binary "/usr/bin/racket")



;;;; c/c++

(defun my-c-mode-common-hook ()
  (require 'irony)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
  (add-to-list 'company-backends 'company-irony))

(defun irony-iotask-ectx-call-callback (ectx result)
  (let ((cb-buffer (irony-iotask-ectx-schedule-buffer ectx)))
    (when (buffer-live-p cb-buffer)
      (with-demoted-errors "Irony I/O task: error in callback: %S"
        (with-current-buffer cb-buffer
          (funcall (irony-iotask-ectx-callback ectx) result))))))

(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)




;;;; custom

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("4c0ef2b0bf8d67f2d82f72d3b1405c33ddbe35fbf704d91c280d5ac48ed69009" default))
 '(package-selected-packages
   '(ddskk csv-mode mozc-popup mozc ac-geiser geiser graphviz-dot-mode company-irony irony intero lsp-ui iflipb lsp-haskell popwin flycheck-pyflakes py-autopep8 flymake-python-pyflakes smart-mode-line python-mode pabbrev helm flymake-cursor company-quickhelp company-jedi)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
