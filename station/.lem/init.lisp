

(in-package :lem-user)

(ql:quickload 'inferior-shell)


;; global

(setf *scroll-recenter-p* nil)
;;(setf (variable-value 'truncate-lines :global) t)

;; theme

(define-color-theme "my-theme" ("emacs-dark")
  (:display-background-mode :dark)
  (:foreground "#ffffff")
  (:background "gray2")
  (:inactive-window-background "gray2"))

(load-theme "my-theme")


;; runcher

(define-command urxvt () ()
  (uiop:run-program "urxvt &"))


;; clipboard

;; (defun xclip-read-clipboard-string ()
;;   (inferior-shell:run/ss '(xclip -o -selection clipboard)))

;; (defun xclip-set-clipboard-string (string)
;;   (inferior-shell:run/ss
;;    `(inferior-shell:pipe (echo (,string)) (xclip -in -selection clipboard))))
 
;; (define-key *global-keymap* "M-y" 'yank-from-xclipboard)
;; (define-command yank-from-xclipboard (&optional arg) ("P")
;;   (declare (ignore arg))
;;   (message (format nil "yank from x-clipboard"))
;;   (setf (buffer-value (current-buffer) 'yank-start) (copy-point (current-point) :temporary))
;;   (let ((string (xclip-read-clipboard-string)))
;;     (insert-string (current-point) string))
;;   (setf (buffer-value (current-buffer) 'yank-end) (copy-point (current-point) :temporary))
;;   (continue-flag :yank)
;;   t)















































